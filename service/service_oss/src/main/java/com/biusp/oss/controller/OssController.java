package com.biusp.oss.controller;

import com.biusp.commonutils.R;
import com.biusp.oss.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author shkstart
 * @create 2021-07-14 19:36
 */
@RestController
@RequestMapping("eduoss")
@CrossOrigin
public class OssController {
    @Resource
    private OssService ossService;
    //上传文件到阿里云oss
    @PostMapping("fileoss")
    public R uploadOssFile(MultipartFile file) {
        //获取上传文件地址
        String url = ossService.upload(file);
        return R.ok().data("url",url);
    }
    @GetMapping("sdf")
    public String sdf(){
        return "dsf";
    }
}
