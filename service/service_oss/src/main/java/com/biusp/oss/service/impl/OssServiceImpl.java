package com.biusp.oss.service.impl;

import cn.hutool.core.util.IdUtil;
import com.biusp.oss.service.OssService;
import com.biusp.oss.utils.ConstantOssPropertiesUtils;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author shkstart
 * @create 2021-07-14 19:36
 */
@Service
public class OssServiceImpl implements OssService {
    //上传文件到腾讯云cos
    @Override
    public String upload(MultipartFile file) {
        // 1 初始化用户身份信息（secretId, secretKey）。
        // Region请按实际情况填写,此处为南京。
        String region_config = ConstantOssPropertiesUtils.REGION;
        String secretKey = ConstantOssPropertiesUtils.SECRET_KEY;
        String secretId = ConstantOssPropertiesUtils.SECRET_ID;
        String bucketName = ConstantOssPropertiesUtils.BUCKET_NAME;
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region(region_config);
        ClientConfig clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);

        // 指定要上传的文件
        File localFile = null;
        try {
            localFile = File.createTempFile("temp",null);
            file.transferTo(localFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 指定文件上传到 COS 上的路径，即对象键。例如对象键为folder/picture.jpg，则表示将文件 picture.jpg 上传到 folder 路径下
        String timeUrl = new DateTime().toString("yyyy/MM/dd");
        String key = "/"+timeUrl+"/"+ IdUtil.simpleUUID() +file.getOriginalFilename();
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        //上传之后文件路径
        String url = "https://edu-guli-1303029870.cos.ap-nanjing.myqcloud.com" + key;
        //返回
        return url;

    }
}
