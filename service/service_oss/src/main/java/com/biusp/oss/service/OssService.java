package com.biusp.oss.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author shkstart
 * @create 2021-07-14 19:35
 */

public interface OssService {

    //上传文件到腾讯云cos
    String upload(MultipartFile file);
}