package com.biusp.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

import javax.annotation.sql.DataSourceDefinition;

/**
 * @author shkstart
 * @create 2021-07-14 19:04
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"com.biusp.oss","com.biusp"})
@EnableDiscoveryClient
public class ossMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(ossMainApplication.class);
    }
}
