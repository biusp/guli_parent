package com.biusp.oss.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author shkstart
 * @create 2021-07-14 19:32
 */
@Component
public class ConstantOssPropertiesUtils implements InitializingBean {

    @Value("${tenxun.oss.secretId}")
    private String secretId;

    @Value("${tenxun.oss.secretKey}")
    private String secretKey;

    @Value("${tenxun.oss.bucketName}")
    private String bucketName;

    @Value("${tenxun.oss.region}")
    private String region;

    public static String SECRET_ID;
    public static String SECRET_KEY;
    public static String BUCKET_NAME;
    public static String REGION;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.SECRET_ID = secretId;
        this.SECRET_KEY = secretKey;
        this.BUCKET_NAME = bucketName;
        this.REGION = region;
    }
}