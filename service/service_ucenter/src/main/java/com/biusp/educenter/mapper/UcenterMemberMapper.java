package com.biusp.educenter.mapper;

import com.biusp.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-08-04
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

}
