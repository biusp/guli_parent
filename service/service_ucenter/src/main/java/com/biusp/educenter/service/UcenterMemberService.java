package com.biusp.educenter.service;

import com.biusp.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.biusp.educenter.entity.vo.RegisterVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-08-04
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember ucenterMember);

    void register(RegisterVo registerVo);
}
