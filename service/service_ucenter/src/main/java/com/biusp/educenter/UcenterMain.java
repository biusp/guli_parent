package com.biusp.educenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author shkstart
 * @create 2021-08-04 9:23
 */
@SpringBootApplication
@ComponentScan("com.biusp")
@MapperScan("com.biusp.educenter.mapper")
public class UcenterMain {
    public static void main(String[] args) {
        SpringApplication.run(UcenterMain.class);
    }
}
