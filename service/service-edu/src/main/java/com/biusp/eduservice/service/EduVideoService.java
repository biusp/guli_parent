package com.biusp.eduservice.service;

import com.biusp.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
public interface EduVideoService extends IService<EduVideo> {

    void deleteByCourseId(String courseId);
}
