package com.biusp.eduservice.controller;

import com.biusp.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author shkstart
 * @create 2021-07-12 20:22
 */
@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin
public class EduLoginController {

    @PostMapping("login")
    public R login(){
        return R.ok().data("token","admin");
    }

    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","[admin]").data("name","admin").data("avatar","https://img0.baidu.com/it/u=3311900507,1448170316&fm=26&fmt=auto&gp=0.jpg");
    }
}
