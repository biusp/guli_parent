package com.biusp.eduservice.client;

import com.biusp.commonutils.R;

/**
 * @author shkstart
 * @create 2021-07-31 17:02
 */
public class VodClientFallback implements VodClient{
    @Override
    public R remove(String id) {
        return R.error().data("erro","出错了!!!");
    }
}
