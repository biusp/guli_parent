package com.biusp.eduservice.entity.subject;

import lombok.Data;

/**
 * @author shkstart
 * @create 2021-07-26 12:42
 */
@Data
public class TwoSubject {
    private String id;
    private String title;
}
