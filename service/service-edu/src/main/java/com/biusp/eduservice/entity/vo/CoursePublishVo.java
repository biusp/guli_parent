package com.biusp.eduservice.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author shkstart
 * @create 2021-07-29 19:08
 */
@ApiModel(value = "课程发布信息")
@Data
public class CoursePublishVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String title;
    private String price;//只用于显示
    private Integer lessonNum;
    private String cover;
    private String subjectLevelOne;
    private String subjectLevelTwo;
    private String teacherName;
}