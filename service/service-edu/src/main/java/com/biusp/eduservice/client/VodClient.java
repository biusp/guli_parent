package com.biusp.eduservice.client;

import com.biusp.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author shkstart
 * @create 2021-07-31 15:43
 */
@FeignClient(name = "service-vod")
@Component
public interface VodClient {

    @DeleteMapping("eduvod/video/removeAlyVideo/{id}")
    R remove(@PathVariable("id") String id);
}
