package com.biusp.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.biusp.eduservice.entity.EduSubject;
import com.biusp.eduservice.entity.excel.SubData;
import com.biusp.eduservice.entity.subject.OneSubject;
import com.biusp.eduservice.entity.subject.TwoSubject;
import com.biusp.eduservice.listener.SubListener;
import com.biusp.eduservice.mapper.EduSubjectMapper;
import com.biusp.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-07-14
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void addSubject(MultipartFile file,EduSubjectService eduSubjectService) {
        try {
            EasyExcel.read(file.getInputStream(), SubData.class,new SubListener(eduSubjectService)).sheet().doRead();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {
        //获取所有一级分类
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id","0");
        List<EduSubject> oneSubjects = baseMapper.selectList(wrapper);

        //获取所有二级分类
        QueryWrapper<EduSubject> twoWrapper= new QueryWrapper<>();
        twoWrapper.ne("parent_id","0");
        List<EduSubject> twoSubjects = baseMapper.selectList(twoWrapper);

        ArrayList<OneSubject> res = new ArrayList<>();

        //封装所有一级二级分类;
        for (EduSubject one : oneSubjects) {
            OneSubject oneSubject = new OneSubject();
            BeanUtils.copyProperties(one,oneSubject);
            res.add(oneSubject);
            ArrayList<TwoSubject> twoRes = new ArrayList<>();
            for (EduSubject two : twoSubjects) {
                if(two.getParentId().equals(one.getId())){
                    TwoSubject twoSubject = new TwoSubject();
                    BeanUtils.copyProperties(two,twoSubject);
                    twoRes.add(twoSubject);
//                    twoSubjects.remove(two);
                }
            }
            oneSubject.setChildren(twoRes);
        }

        return res;
    }
}
