package com.biusp.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.biusp.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.biusp.eduservice.entity.vo.TeacherQuery;

import java.util.List;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-06-28
 */
public interface EduTeacherService extends IService<EduTeacher> {
    void pageQuery(Page<EduTeacher> pageParam, TeacherQuery teacherQuery);

    List<EduTeacher> getList();
}
