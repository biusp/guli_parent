package com.biusp.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.biusp.eduservice.entity.EduCourse;
import com.biusp.eduservice.entity.EduCourseDescription;
import com.biusp.eduservice.entity.EduTeacher;
import com.biusp.eduservice.entity.vo.CourseInfoVo;
import com.biusp.eduservice.entity.vo.CoursePublishVo;
import com.biusp.eduservice.entity.vo.CourseQuery;
import com.biusp.eduservice.mapper.EduCourseMapper;
import com.biusp.eduservice.service.EduChapterService;
import com.biusp.eduservice.service.EduCourseDescriptionService;
import com.biusp.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.biusp.eduservice.service.EduVideoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {
    @Autowired
    EduCourseDescriptionService service;

    @Autowired
    EduVideoService eduVideoService;

    @Autowired
    EduChapterService eduChapterService;

    @Autowired
    EduCourseDescriptionService eduCourseDescriptionService;

    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int insert = baseMapper.insert(eduCourse);
        if(insert == 0){
            throw new RuntimeException("添加课程失败");
        }

        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        BeanUtils.copyProperties(courseInfoVo,eduCourseDescription);
        eduCourseDescription.setId(eduCourse.getId());
        service.save(eduCourseDescription);
        return eduCourse.getId();

    }

    @Override
    public CourseInfoVo getCourseInfo(String courseId) {
        EduCourse eduCourse = baseMapper.selectById(courseId);

        EduCourseDescription description = service.getById(courseId);

        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(eduCourse,courseInfoVo);
        courseInfoVo.setDescription(description.getDescription());
        return courseInfoVo;
    }

    @Override
    public void updateCourseInfo(CourseInfoVo courseInfoVo) {
        //1 修改课程表
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int update = baseMapper.updateById(eduCourse);
        if(update == 0) {
            throw new RuntimeException("修改课程信息失败");
        }

        //2 修改描述表
        EduCourseDescription description = new EduCourseDescription();
        description.setId(courseInfoVo.getId());
        description.setDescription(courseInfoVo.getDescription());
        service.updateById(description);
    }

    @Override
    public CoursePublishVo publishCourseInfo(String id) {
        return baseMapper.selectCoursePublishVoById(id);
    }

    //todo 删除阿里云视频
    @Override
    public void deleteCourseById(String courseId) {
        //删除课程
        baseMapper.deleteById(courseId);

        //删除章节
        eduChapterService.deleteByCourseId(courseId);
        //删除小结
        eduVideoService.deleteByCourseId(courseId);

        //删除课程描述
        eduCourseDescriptionService.deleteByCourseId(courseId);

    }

    @Override
    public void pageQuery(Page<EduCourse> ipage, CourseQuery courseQuery) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        if (courseQuery == null){
            baseMapper.selectPage(ipage, wrapper);
            return;
        }
        String status = courseQuery.getStatus();
        String title = courseQuery.getTitle();
        if(!StringUtils.isEmpty(title)){
            wrapper.like("title",title);
        }
        if(!StringUtils.isEmpty(status)){
            wrapper.eq("status",status);
        }

        baseMapper.selectPage(ipage,wrapper);

    }

    @Cacheable(value = "EduCourseList")
    @Override
    public List<EduCourse> getList() {
        //查询前8条热门课程
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        wrapper.last("limit 8");
        List<EduCourse> eduList = baseMapper.selectList(wrapper);
        return eduList;
    }
}
