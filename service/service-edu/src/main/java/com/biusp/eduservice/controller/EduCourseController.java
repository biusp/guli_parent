package com.biusp.eduservice.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.biusp.commonutils.R;
import com.biusp.eduservice.entity.EduCourse;
import com.biusp.eduservice.entity.EduTeacher;
import com.biusp.eduservice.entity.vo.CourseInfoVo;
import com.biusp.eduservice.entity.vo.CoursePublishVo;
import com.biusp.eduservice.entity.vo.CourseQuery;
import com.biusp.eduservice.entity.vo.TeacherQuery;
import com.biusp.eduservice.service.EduCourseService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/eduservice/course")
@CrossOrigin
public class EduCourseController {

    @Autowired
    EduCourseService eduCourseService;


    @GetMapping
    public R list(){
        List<EduCourse> list = eduCourseService.list(null);
        return R.ok().data("list",list);
    }

    @PostMapping("addCourseInfo")
    public R addCourseInfo(@RequestBody CourseInfoVo courseInfoVo){
        String id = eduCourseService.saveCourseInfo(courseInfoVo);
        return R.ok().data("courseId",id);
    }
    //根据课程id查询课程基本信息
    @GetMapping("getCourseInfo/{courseId}")
    public R getCourseInfo(@PathVariable String courseId) {
        CourseInfoVo courseInfoVo = eduCourseService.getCourseInfo(courseId);
        return R.ok().data("courseInfoVo",courseInfoVo);
    }

    //修改课程信息
    @PostMapping("updateCourseInfo")
    public R updateCourseInfo(@RequestBody CourseInfoVo courseInfoVo) {
        eduCourseService.updateCourseInfo(courseInfoVo);
        return R.ok();
    }

    @ApiOperation(value = "根据ID获取课程发布信息")
    @GetMapping("getPublishCourseInfo/{id}")
    public R getCoursePublishVoById(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id){
        CoursePublishVo courseInfoForm = eduCourseService.publishCourseInfo(id);
        return R.ok().data("publishCourse", courseInfoForm);
    }

    @PostMapping("publishCourse/{id}")
    public R publihCourse(@PathVariable String id){
        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(id);
        eduCourse.setStatus("Normal");
        eduCourseService.updateById(eduCourse);
        return R.ok();
    }

    @DeleteMapping("{courseId}")
    public R delete(@PathVariable String courseId){
        eduCourseService.deleteCourseById(courseId);
        return R.ok();
    }


    @ApiOperation(value = "分页课程列表")
    @PostMapping("getCourseListPage/{page}/{limit}")
    public R getCourseListPage(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "teacherQuery", value = "查询对象", required = false)
            @RequestBody(required = false)
                    CourseQuery courseQuery){


        Page<EduCourse> Ipage = new Page(page,limit);
        eduCourseService.pageQuery(Ipage, courseQuery);
        List<EduCourse> records = Ipage.getRecords();
        long total = Ipage.getTotal();
        return R.ok().data("total", total).data("list", records);
    }
}

