package com.biusp.eduservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.biusp.commonutils.R;
import com.biusp.eduservice.entity.EduTeacher;
import com.biusp.eduservice.entity.vo.TeacherQuery;
import com.biusp.eduservice.service.EduTeacherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-06-28
 */
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class EduTeacherController {
    @Autowired
    EduTeacherService teacherService;

    @GetMapping("findAll")
    public R findAll(){
        List<EduTeacher> list = teacherService.list(null);
        return R.ok().data("items", list);
    }

    @DeleteMapping("{id}")
    public R removeById(@PathVariable String id){

        teacherService.removeById(id);
        return R.ok();
    }

    @ApiOperation(value = "分页讲师列表")
    @PostMapping("pageTeacherCondition/{page}/{limit}")
    public R pageList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "teacherQuery", value = "查询对象", required = false)
            @RequestBody(required = false)
                    TeacherQuery teacherQuery){


        Page<EduTeacher> Ipage = new Page(page,limit);
        teacherService.pageQuery(Ipage, teacherQuery);
        List<EduTeacher> records = Ipage.getRecords();
        long total = Ipage.getTotal();
        return R.ok().data("total", total).data("rows", records);
    }

    @PostMapping("addTeacher")
    @ApiOperation(value = "新增讲师")
    public R save(
            @ApiParam(name = "teacher", value = "讲师对象", required = true)
            @RequestBody EduTeacher teacher){
        boolean save = teacherService.save(teacher);
        if(save)
            return R.ok();
        else
            return R.error();
    }

    @ApiOperation(value = "根据ID查询讲师")
    @GetMapping("{id}")
    public R getById(
            @ApiParam(name = "id", value = "讲师ID", required = true)
            @PathVariable String id){
        EduTeacher teacher = teacherService.getById(id);
        return R.ok().data("teacher", teacher);
    }
    @ApiOperation(value = "更新讲师")
    @PostMapping("updateTeacher")
    public R updateTeacher(
            @ApiParam(name = "teacher", value = "讲师对象", required = true)
            @RequestBody EduTeacher teacher){
        boolean b = teacherService.updateById(teacher);
        if(b)
            return R.ok();
        else
            return R.error();
    }


}

