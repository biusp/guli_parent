package com.biusp.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.biusp.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.biusp.eduservice.entity.vo.CourseInfoVo;
import com.biusp.eduservice.entity.vo.CoursePublishVo;
import com.biusp.eduservice.entity.vo.CourseQuery;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseInfo(String courseId);

    void updateCourseInfo(CourseInfoVo courseInfoVo);

    CoursePublishVo publishCourseInfo(String id);

    void deleteCourseById(String courseId);

    void pageQuery(Page<EduCourse> ipage, CourseQuery courseQuery);

    List<EduCourse> getList();
}
