package com.biusp.eduservice.mapper;

import com.biusp.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-06-28
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
