package com.biusp.eduservice.controller;


import com.biusp.commonutils.R;
import com.biusp.eduservice.client.VodClient;
import com.biusp.eduservice.entity.EduVideo;
import com.biusp.eduservice.entity.chapter.VideoVo;
import com.biusp.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/eduservice/video")
@CrossOrigin
public class EduVideoController {
    @Autowired
    private EduVideoService eduVideoService;

    @Autowired
    VodClient vodClient;

    @PostMapping("addVideo")
    public R add(@RequestBody EduVideo eduVideo){
        eduVideoService.save(eduVideo);
        return R.ok();
    }

    //删除小结并阿里云视频点播
    @DeleteMapping("{id}")
    public R delete(@PathVariable String id){
        //获取对应的小结
        EduVideo byId = eduVideoService.getById(id);
        String videoSourceId = byId.getVideoSourceId();
        //通过小结得到视频id并远程调用删除
        if(!StringUtils.isEmpty(videoSourceId))
        vodClient.remove(videoSourceId);
        eduVideoService.removeById(id);
        return R.ok();
    }




}

