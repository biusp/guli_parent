package com.biusp.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.biusp.eduservice.entity.EduSubject;
import com.biusp.eduservice.entity.excel.SubData;
import com.biusp.eduservice.service.EduSubjectService;

/**
 * @author shkstart
 * @create 2021-07-26 10:15
 */
public class SubListener extends AnalysisEventListener<SubData> {
    private EduSubjectService service;

    public SubListener(EduSubjectService service) {
        this.service = service;
    }

    @Override
    public void invoke(SubData subData, AnalysisContext analysisContext) {
        if(subData == null)
            throw new RuntimeException("数据为空");

        //添加一级分类
        String oneSubjectName = subData.getOneSubjectName();
        EduSubject oneSubject = existOneSubject(oneSubjectName);
        if(oneSubject == null){
            oneSubject = new EduSubject();
            oneSubject.setTitle(oneSubjectName);
            oneSubject.setParentId("0");
            service.save(oneSubject);
        }
        //添加二级分类

        EduSubject twoSubject = existTwoSubject(subData.getTowSubjectName(), oneSubject.getId());
        if(twoSubject == null){
            twoSubject = new EduSubject();
            twoSubject.setTitle(subData.getTowSubjectName());
            twoSubject.setParentId(oneSubject.getId());
            service.save(twoSubject);
        }


    }
    //判断一级分类是否重复
    private EduSubject existOneSubject(String oneSubjectName){
        QueryWrapper<EduSubject> w = new QueryWrapper<>();
        w.eq("title",oneSubjectName);
        w.eq("parent_id","0");
        EduSubject one = service.getOne(w);
        return one;
    }
    //判断二级分类是否重复
    private EduSubject existTwoSubject(String name,String pId){
        QueryWrapper<EduSubject> w = new QueryWrapper<>();
        w.eq("title",name);
        w.eq("parent_id",pId);
        EduSubject two = service.getOne(w);
        return two;
    }
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
