package com.biusp.eduservice.controller;


import com.biusp.commonutils.R;
import com.biusp.eduservice.entity.subject.OneSubject;
import com.biusp.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-07-14
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
public class EduSubjectController {

    @Autowired
    EduSubjectService eduSubjectService;

    @PostMapping("addSubject")
    public R addSubject(MultipartFile file){
        eduSubjectService.addSubject(file,eduSubjectService);
        return R.ok();
    }


    @GetMapping("getAllSubject")
    public R getAllSubject(){
        List<OneSubject> res = eduSubjectService.getAllOneTwoSubject();
        return R.ok().data("list",res);
    }

}

