package com.biusp.eduservice.service;

import com.biusp.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.biusp.eduservice.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
public interface EduChapterService extends IService<EduChapter> {
    //课程大纲列表,根据课程id进行查询
    List<ChapterVo> getChapterVideoByCourseId(String courseId);

    boolean deleteChapter(String chapterId);

    void deleteByCourseId(String courseId);
}
