package com.biusp.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author shkstart
 * @create 2021-07-26 10:19
 */
@Data
public class SubData {
    @ExcelProperty(index = 0)
    private String oneSubjectName;
    @ExcelProperty(index = 1)
    private String towSubjectName;
}
