package com.biusp.eduservice.entity.chapter;

import lombok.Data;

/**
 * @author shkstart
 * @create 2021-07-29 15:41
 */
@Data
public class VideoVo {

    private String id;

    private String title;
}
