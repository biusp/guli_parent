package com.biusp.eduservice.mapper;

import com.biusp.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-07-26
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
