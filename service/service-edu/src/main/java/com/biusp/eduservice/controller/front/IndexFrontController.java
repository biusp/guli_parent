package com.biusp.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.biusp.commonutils.R;
import com.biusp.eduservice.entity.EduCourse;
import com.biusp.eduservice.entity.EduTeacher;
import com.biusp.eduservice.service.EduCourseService;
import com.biusp.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/eduservice/indexfront")
@CrossOrigin
public class IndexFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduTeacherService teacherService;

    //查询前8条热门课程，查询前4条名师
    @GetMapping("index")
    public R index() {

        List<EduCourse> eduList = courseService.getList();


        List<EduTeacher> teacherList = teacherService.getList();
        return R.ok().data("eduList",eduList).data("teacherList",teacherList);
    }

}
