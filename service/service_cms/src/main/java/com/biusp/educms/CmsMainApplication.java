package com.biusp.educms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author shkstart
 * @create 2021-08-01 15:16
 */
@SpringBootApplication
@ComponentScan("com.biusp")
@MapperScan("com.biusp.educms.mapper")
public class CmsMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmsMainApplication.class);
    }
}
