package com.biusp.vod.service;

import com.aliyuncs.exceptions.ClientException;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author shkstart
 * @create 2021-07-30 18:29
 */
public interface VodService {
    String uploadAlyiVideo(MultipartFile file);

    void removeAlyVideo(String id) throws ClientException;
}
