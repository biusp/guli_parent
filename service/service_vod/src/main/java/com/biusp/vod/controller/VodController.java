package com.biusp.vod.controller;

import com.aliyuncs.exceptions.ClientException;
import com.biusp.commonutils.R;
import com.biusp.vod.service.VodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.GET;

/**
 * @author shkstart
 * @create 2021-07-30 18:29
 */
@RestController
@RequestMapping("eduvod/video")
@CrossOrigin
public class VodController {

    @Autowired
    VodService vodService;


    //上传视频到阿里云
    @PostMapping("uploadAlyiVideo")
    public R upload(MultipartFile file){
        String id = vodService.uploadAlyiVideo(file);
        return R.ok().data("videoId",id);
    }

    @DeleteMapping("removeAlyVideo/{id}")
    public R remove(@PathVariable String id){
        try {
            vodService.removeAlyVideo(id);
            return R.ok();
        } catch (ClientException e) {
            throw new RuntimeException("删除失败");
        }
    }

}
