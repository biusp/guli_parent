package com.biusp.vod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author shkstart
 * @create 2021-07-30 18:27
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"com.biusp"})
@EnableDiscoveryClient
public class VodMain {
    public static void main(String[] args) {
        SpringApplication.run(VodMain.class);
    }
}
