package com.biusp.msm.controller;

import com.biusp.commonutils.R;
import com.biusp.msm.service.MsmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author shkstart
 * @create 2021-08-02 11:14
 */
@RestController
@RequestMapping("/edumsm/msm")
@CrossOrigin //跨域
public class MsmApiController {
    @Autowired
    private MsmService msmService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping(value = "/send/{phone}")
    public R code(@PathVariable String phone) {
        String code = redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(code))
            return R.ok();
        code = "6666";
        redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.MINUTES);
        return R.ok();

    }
}
