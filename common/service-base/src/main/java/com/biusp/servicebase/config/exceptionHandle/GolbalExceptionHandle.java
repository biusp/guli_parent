package com.biusp.servicebase.config.exceptionHandle;

import com.biusp.commonutils.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GolbalExceptionHandle {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R handle(Exception e){
        e.printStackTrace();
        return R.error().message("全局异常处理...");
    }
}
